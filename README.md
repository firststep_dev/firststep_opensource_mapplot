# Title

firststep_opensource_mapplot

# Posted by

Leen Remmelzwaal (leen@firststep.ai)

# StackOverFlow Post:

https://stackoverflow.com/questions/53233228/plot-latitude-longitude-from-csv-in-python-3-6/74105923#74105923

# Requirements

`python3 -m pip install pandas`

`python3 -m pip install plotly`

# Run

`python3 main.py`

# Output

Example output: output.png
